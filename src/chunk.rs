use {
    std::{
        collections::HashMap,
    },
    specs::Entity,
    cgmath::{prelude::*, Vector3, Vector2, Point2, Point3},
    ndarray::Array3,
};

use crate::{
    thread_ref::ThreadRef,
    prim::{Ray, Aabb},
    math::EuclidElementWise,
};

pub type Cell = u32;

pub const CHUNK_SIZE: Vector3<usize> = Vector3 { x: 64, y: 64, z: 32 };

pub const CHUNK_WORLD_SIZE: Vector3<f32> = Vector3 {
    x: CHUNK_SIZE.x as f32 * CELL_SIZE.x,
    y: CHUNK_SIZE.y as f32 * CELL_SIZE.y,
    z: CHUNK_SIZE.z as f32 * CELL_SIZE.z,
};

pub const CHUNK_VIEW_RADIUS: f32 = 240.;

pub const CELL_SIZE: Vector3<f32> = Vector3 { x: 1., y: 1., z: 0.5 };

fn world_to_cell(pos: Point3<f32>) -> Point3<i32> {
    Point3::from_vec(pos.to_vec().div_euclid_element_wise(CELL_SIZE).cast::<i32>().unwrap())
}

fn world_to_chunk(pos: Point3<f32>) -> Point3<i32> {
    Point3::from_vec(pos.to_vec().div_euclid_element_wise(CHUNK_WORLD_SIZE).cast::<i32>().unwrap())
}

fn world_to_chunk_2d(pos: Point2<f32>) -> Point2<i32> {
    Point2::from_vec(pos.to_vec().div_euclid_element_wise(CHUNK_WORLD_SIZE.truncate()).cast::<i32>().unwrap())
}

fn cell_to_chunk(pos: Point3<i32>) -> Point3<i32> {
    Point3::from_vec(pos.to_vec().div_euclid_element_wise(CHUNK_SIZE.cast::<i32>().unwrap()))
}

pub fn chunk_center(pos: Point2<i32>) -> Point3<f32> {
    Point3::new(
        (pos.x as f32 + 0.5) * CHUNK_WORLD_SIZE.x,
        (pos.y as f32 + 0.5) * CHUNK_WORLD_SIZE.y,
        CHUNK_WORLD_SIZE.z * 0.5)
}

fn chunk_flat_center(pos: Point2<i32>) -> Point2<f32> {
    Point2::new(
        (pos.x as f32 + 0.5) * CHUNK_WORLD_SIZE.x,
        (pos.y as f32 + 0.5) * CHUNK_WORLD_SIZE.y)
}

fn chunk_start(pos: Point2<i32>) -> Point3<f32> {
    Point3::new(
        pos.x as f32 * CHUNK_SIZE.x as f32 * CELL_SIZE.x,
        pos.y as f32 * CHUNK_SIZE.y as f32 * CELL_SIZE.y,
        0.)
}

pub fn cell_aabb(pos: Point3<i32>) -> Aabb {
    Aabb::new(cell_center(pos), CELL_SIZE * 0.5)
}

pub fn cell_center(pos: Point3<i32>) -> Point3<f32> {
    Point3::from_vec(pos.to_vec().cast::<f32>().unwrap().mul_element_wise(CELL_SIZE) + 0.5 * CELL_SIZE)
}

fn clamp(a: Point3<i32>, min: Point3<i32>, max: Point3<i32>) -> Point3<i32> {
    fn c(a: i32, min: i32, max: i32) -> i32 { a.max(min).min(max) }
    Point3::new(c(a.x, min.x, max.x), c(a.y, min.y, max.y), c(a.z, min.z, max.z))
}

pub struct Chunk {
    pub start: Point3<f32>,
    pub center: Point3<f32>,
    pub data: Array3<Cell>,
    pub tess: Option<ThreadRef<luminance::tess::Tess>>,
    pub entity: Option<Entity>,
}

impl Chunk {
    pub fn aabb(&self) -> Aabb {
        Aabb::new(self.center, CHUNK_WORLD_SIZE * 0.5)
    }

    pub fn intersect_aabb(&self, aabb: Aabb) -> Option<(Point3<i32>, Vector3<i32>)> {
        let end = Point3::from_vec(CHUNK_SIZE.cast::<i32>().unwrap() - Vector3::new(1, 1, 1));
        let cell_start = clamp(world_to_cell(aabb.start() - self.start.to_vec()), Point3::origin(), end);
        let cell_end = clamp(world_to_cell(aabb.end() - self.start.to_vec()), Point3::origin(), end);
        let size = cell_end - cell_start;
        if size.x >= 0 && size.y >= 0 && size.z >= 0 {
            Some((cell_start, size))
        }
        else {
            None
        }
    }
}

pub struct ChunkManager {
    pub chunks: HashMap<Point2<i32>, Chunk>,
}

impl ChunkManager {
    pub fn is_loaded(&self, pos: Point2<i32>) -> bool {
        self.chunks.contains_key(&pos)
    }

    pub fn load(&mut self, pos: Point2<i32>) -> bool {
        if self.is_loaded(pos) {
            return false;
        }
        let start = chunk_start(pos);
        let center = chunk_center(pos);
        let data = Array3::from_shape_fn((CHUNK_SIZE.x, CHUNK_SIZE.y, CHUNK_SIZE.z), |(_x, _y, z)| if z == 0 { 1 } else { 0 });
        let chunk = Chunk {
            start,
            center,
            data,
            tess: None,
            entity: None,
        };
        self.chunks.insert(pos, chunk);
        true
    }

    pub fn maintain_radius(&mut self, pos: Point2<f32>, radius: f32, mut load_cb: impl FnMut(Point2<i32>) -> Entity, mut unload_cb: impl FnMut(Entity)) {
        let radius_vec = Vector2::new(radius, radius);
        let a = pos - radius_vec;
        let b = pos + radius_vec;
        let chunk_a = world_to_chunk_2d(a);
        let chunk_b = world_to_chunk_2d(b);
        for iy in chunk_a.y .. chunk_b.y {
            for ix in chunk_a.x .. chunk_b.x {
                let chunk_pos = Point2::new(ix, iy);
                let chunk_center = chunk_flat_center(chunk_pos);
                if chunk_center.distance(pos) <= radius {
                    if self.load(chunk_pos) {
                        self.chunks.get_mut(&chunk_pos).unwrap().entity = Some(load_cb(chunk_pos));
                    }
                }
            }
        }
        self.chunks.retain(|&k, c|
            if chunk_flat_center(k).distance(pos) <= radius {
                true
            } else {
                if let Some(e) = c.entity {
                    unload_cb(e);
                }
                false
            });
    }

    pub fn cast_ray(&self, ray: Ray) -> Option<(Point2<i32>, Point3<usize>, Point3<f32>, Vector3<f32>)> {
        let mut cur = world_to_cell(ray.origin);
        let step = ray.dir.map(|x| x.signum()).cast::<i32>().unwrap();
        let mut t_max = ray.origin.to_vec().rem_euclid_element_wise(CELL_SIZE);
        if step.x == 1 { t_max.x = CELL_SIZE.x - t_max.x; }
        if step.y == 1 { t_max.y = CELL_SIZE.y - t_max.y; }
        if step.z == 1 { t_max.z = CELL_SIZE.z - t_max.z; }
        let dir_abs = ray.dir.map(|x| x.abs());
        t_max = t_max.div_element_wise(dir_abs);
        let t_delta = CELL_SIZE.div_element_wise(dir_abs);
        for _ in 0..256 { // TODO: maybe use a distance limit condition
            if t_max.x < t_max.y {
                if t_max.x < t_max.z {
                    cur.x += step.x;
                    t_max.x += t_delta.x;
                }
                else {
                    cur.z += step.z;
                    t_max.z += t_delta.z;
                }
            }
            else if t_max.y < t_max.z {
                cur.y += step.y;
                t_max.y += t_delta.y;
            }
            else {
                cur.z += step.z;
                t_max.z += t_delta.z;
            }
            let cur_chunk = cell_to_chunk(cur);
            if cur_chunk.z < 0 && step.z < 0 {
                return None;
            }
            if cur_chunk.z > 1 && step.z > 0 {
                return None;
            }
            if cur_chunk.z == 0 {
                if let Some(chunk) = self.chunks.get(&Point2::new(cur_chunk.x, cur_chunk.y)) {
                    let cur_chunk_start = Point3::origin() + cur_chunk.to_vec().mul_element_wise(CHUNK_SIZE.cast::<i32>().unwrap());
                    let cur_in_chunk = (cur - cur_chunk_start).cast::<usize>().unwrap();
                    if chunk.data[(cur_in_chunk.x, cur_in_chunk.y, cur_in_chunk.z)] > 0 {
                        let chunk_coord = Point2::new(cur_chunk.x, cur_chunk.y);
                        let cell_coord = Point3::origin() + cur_in_chunk;
                        let aabb = cell_aabb(cur);
                        if let Some(hit_pos) = ray.intersect_aabb(aabb) {
                            let normal = aabb.normal_at(hit_pos);
                            return Some((chunk_coord, cell_coord, hit_pos, normal));
                        }
                    }
                }
            }
        }
        None
    }

    pub fn get_mut(&mut self, pos: Point2<i32>) -> Option<&mut Chunk> {
        self.chunks.get_mut(&pos).map(|c| { c.tess = None; c })
    }
}

impl Default for ChunkManager {
    fn default() -> ChunkManager {
        ChunkManager {
            chunks: HashMap::new(),
        }
    }
}
