use {
    cgmath::{
        prelude::*,
        Vector2,
        Point2,
    },
};

use super::{
    shaders::UiVertex,
};

pub struct Canvas {
    size: Vector2<f32>,
    commands: Vec<Command>,
    color: [u8; 4],
    thickness: f32,
}

impl Canvas {
    pub fn new(size: Vector2<f32>) -> Canvas {
        Canvas {
            size,
            commands: Vec::new(),
            color: [0, 0, 0, 255],
            thickness: 1.,
        }
    }

    pub fn size(&self) -> Vector2<f32> {
        self.size
    }
    
    pub fn set_color(&mut self, color: [u8; 4]) {
        self.color = color;
    }

    pub fn set_thickness(&mut self, thickness: f32) {
        self.thickness = thickness;
    }

    pub fn line(&mut self, a: Point2<f32>, b: Point2<f32>) {
        self.commands.push(Command::Line(self.color, self.thickness, a, b));
    }

    pub fn fill_rect(&mut self, start: Point2<f32>, size: Vector2<f32>) {
        self.commands.push(Command::FillRect(self.color, start, size));
    }

    pub fn into_commands(self) -> Vec<Command> {
        self.commands
    }
}

#[derive(Debug, Clone, Copy)]
pub enum Command {
    Line([u8; 4], f32, Point2<f32>, Point2<f32>),
    FillRect([u8; 4], Point2<f32>, Vector2<f32>),
}

impl Command {
    pub fn write_to(self, out: &mut Vec<UiVertex>) {
        match self {
            Command::Line(color, thickness, start, end) => {
                let diff = start - end;
                let perp = Vector2::new(diff.y, diff.x);
                let side = perp.normalize_to(thickness);
                out.extend_from_slice(&[
                    UiVertex::new_solid(start + side, Point2::new(0., 0.), color),
                    UiVertex::new_solid(end + side, Point2::new(1., 0.), color),
                    UiVertex::new_solid(end - side, Point2::new(1., 1.), color),
                    UiVertex::new_solid(end - side, Point2::new(1., 1.), color),
                    UiVertex::new_solid(start - side, Point2::new(0., 1.), color),
                    UiVertex::new_solid(start + side, Point2::new(0., 0.), color),
                ]);
            },
            Command::FillRect(color, start, size) => {
                let s = start;
                let e = start + size;
                out.extend_from_slice(&[
                    UiVertex::new_solid(Point2::new(s.x, s.y), Point2::new(0., 0.), color),
                    UiVertex::new_solid(Point2::new(e.x, s.y), Point2::new(1., 0.), color),
                    UiVertex::new_solid(Point2::new(e.x, e.y), Point2::new(1., 1.), color),
                    UiVertex::new_solid(Point2::new(e.x, e.y), Point2::new(1., 1.), color),
                    UiVertex::new_solid(Point2::new(s.x, e.y), Point2::new(0., 1.), color),
                    UiVertex::new_solid(Point2::new(s.x, s.y), Point2::new(0., 0.), color),
                ]);
            },
        }
    }
}

pub fn draw_ui(canvas: &mut Canvas) {
    let panel_size = Vector2::new(200., 32.);
    let panel_start = Point2::new(canvas.size().x * 0.5 - panel_size.x * 0.5, canvas.size().y - panel_size.y);
    canvas.set_color([90, 90, 150, 255]);
    canvas.fill_rect(panel_start, panel_size);
    canvas.set_color([100, 100, 240, 255]);
    canvas.set_thickness(2.);
    canvas.line(panel_start, panel_start + Vector2::new(panel_size.x, 0.));
    canvas.line(panel_start, panel_start + Vector2::new(0., panel_size.y));
    canvas.line(panel_start + Vector2::new(panel_size.x, 0.), panel_start + panel_size);
}
