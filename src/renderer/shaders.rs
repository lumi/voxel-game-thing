use {
    luminance::{
        shader::program::{Program, Uniform},
        texture::{Flat, Dim2},
        pipeline::BoundTexture,
        pixel::Floating,
    },
    luminance_derive::{
        UniformInterface,
        Semantics,
        Vertex,
    },
    cgmath::{
        Point3,
        Point2,
        Vector3,
    },
    std::fs,
};

#[derive(Clone, Copy, Debug, Eq, PartialEq, Semantics)]
pub enum Semantics {
    #[sem(name = "co", repr = "[f32; 3]", wrapper = "VertexPosition")]
    Position,
    #[sem(name = "normal", repr = "[f32; 3]", wrapper = "VertexNormal")]
    Normal,
    #[sem(name = "uv", repr = "[f32; 2]", wrapper = "VertexUv")]
    Uv,
    #[sem(name = "color", repr = "[u8; 3]", wrapper = "VertexColor")]
    Color,
}

#[derive(Debug, UniformInterface)]
pub struct ShaderInterface {
    #[uniform(name = "view")]
    pub view_matrix: Uniform<[[f32; 4]; 4]>,
    #[uniform(name = "perspective")]
    pub perspective_matrix: Uniform<[[f32; 4]; 4]>,
    #[uniform(name = "model")]
    pub model_matrix: Uniform<[[f32; 4]; 4]>,
    #[uniform(name = "light")]
    pub light_dir: Uniform<[f32; 3]>,
}

#[repr(C)]
#[derive(Clone, Copy, Debug, PartialEq, Vertex)]
#[vertex(sem = "Semantics")]
pub struct Vertex {
    pub pos: VertexPosition,
    pub normal: VertexNormal,
    pub uv: VertexUv,
    #[vertex(normalized = "true")]
    pub rgb: VertexColor,
}

impl Vertex {
    pub fn make(pos: Point3<f32>, normal: Vector3<f32>, uv: Point2<f32>, rgb: [u8; 3]) -> Vertex {
        Vertex::new(VertexPosition::new(pos.into()), VertexNormal::new(normal.into()), VertexUv::new(uv.into()), VertexColor::new(rgb))
    }
}

pub const TERRAIN_VERTEX_SHADER_PATH: &'static str = "assets/shaders/terrain.vertex.glsl";
pub const TERRAIN_FRAGMENT_SHADER_PATH: &'static str = "assets/shaders/terrain.fragment.glsl";

pub type TerrainProgram = Program<Semantics, (), ShaderInterface>;

pub fn make_terrain_program() -> TerrainProgram {
    let vertex_shader = fs::read_to_string(TERRAIN_VERTEX_SHADER_PATH).unwrap();
    let fragment_shader = fs::read_to_string(TERRAIN_FRAGMENT_SHADER_PATH).unwrap();
    TerrainProgram::from_strings(None, &vertex_shader, None, &fragment_shader).expect("error loading terrain shader").ignore_warnings()
}

pub const ENTITY_VERTEX_SHADER_PATH: &'static str = "assets/shaders/entity.vertex.glsl";
pub const ENTITY_FRAGMENT_SHADER_PATH: &'static str = "assets/shaders/entity.fragment.glsl";

pub type EntityProgram = Program<Semantics, (), ShaderInterface>;

pub fn make_entity_program() -> EntityProgram {
    let vertex_shader = fs::read_to_string(ENTITY_VERTEX_SHADER_PATH).unwrap();
    let fragment_shader = fs::read_to_string(ENTITY_FRAGMENT_SHADER_PATH).unwrap();
    EntityProgram::from_strings(None, &vertex_shader, None, &fragment_shader).expect("error loading entity shader").ignore_warnings()
}

#[derive(Clone, Copy, Debug, Eq, PartialEq, Semantics)]
pub enum UiSemantics {
    #[sem(name = "co", repr = "[f32; 2]", wrapper = "UiVertexPosition")]
    Position,
    #[sem(name = "uv", repr = "[f32; 2]", wrapper = "UiVertexUv")]
    Uv,
    #[sem(name = "color", repr = "[u8; 4]", wrapper = "UiVertexColor")]
    Color,
    #[sem(name = "mode", repr = "u8", wrapper = "UiVertexMode")]
    Mode,
}

#[derive(UniformInterface)]
pub struct UiShaderInterface {
    #[uniform(name = "transform")]
    pub transform_matrix: Uniform<[[f32; 3]; 3]>,
    pub window_size: Uniform<[f32; 2]>,
    #[uniform(unbound)]
    pub tex: Uniform<&'static BoundTexture<'static, Flat, Dim2, Floating>>,
}

#[repr(C)]
#[derive(Clone, Copy, Debug, PartialEq, Vertex)]
#[vertex(sem = "UiSemantics")]
pub struct UiVertex {
    pub pos: UiVertexPosition,
    pub uv: UiVertexUv,
    #[vertex(normalized = "true")]
    pub color: UiVertexColor,
    pub mode: UiVertexMode,
}

impl UiVertex {
    pub fn new_solid(pos: Point2<f32>, uv: Point2<f32>, color: [u8; 4]) -> UiVertex {
        UiVertex::new(UiVertexPosition::new(pos.into()), UiVertexUv::new(uv.into()), UiVertexColor::new(color), UiVertexMode::new(1))
    }

    pub fn new_texture(pos: Point2<f32>, uv: Point2<f32>) -> UiVertex {
        UiVertex::new(UiVertexPosition::new(pos.into()), UiVertexUv::new(uv.into()), UiVertexColor::new([0, 0, 0, 255]), UiVertexMode::new(0))
    }
}

pub const UI_VERTEX_SHADER_PATH: &'static str = "assets/shaders/ui.vertex.glsl";
pub const UI_FRAGMENT_SHADER_PATH: &'static str = "assets/shaders/ui.fragment.glsl";

pub type UiProgram = Program<UiSemantics, (), UiShaderInterface>;

pub fn make_ui_program() -> UiProgram {
    let vertex_shader = fs::read_to_string(UI_VERTEX_SHADER_PATH).unwrap();
    let fragment_shader = fs::read_to_string(UI_FRAGMENT_SHADER_PATH).unwrap();
    UiProgram::from_strings(None, &vertex_shader, None, &fragment_shader).expect("error loading ui shader").ignore_warnings()
}
