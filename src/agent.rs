use {
    cgmath::Point3,
};

#[derive(Debug, Clone)]
pub enum Order {
    MoveTo(Point3<f32>),
}
