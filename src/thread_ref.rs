use {
    std::{
        thread::{self, ThreadId},
        ops::{Deref, DerefMut},
    },
};

pub struct ThreadRef<T>{
    inner: T,
    thread_id: ThreadId,
}

impl<T> ThreadRef<T> {
    pub fn new(inner: T) -> ThreadRef<T> {
        let thread_id = thread::current().id();
        ThreadRef { inner, thread_id }
    }
}

impl<T> Deref for ThreadRef<T> {
    type Target = T;

    fn deref(&self) -> &T {
        if thread::current().id() != self.thread_id {
            panic!("dereferencing ThreadRef outside of its thread")
        }
        else {
            &self.inner
        }
    }
}

impl<T> DerefMut for ThreadRef<T> {
    fn deref_mut(&mut self) -> &mut T {
        if thread::current().id() != self.thread_id {
            panic!("dereferencing ThreadRef outside of its thread")
        }
        else {
            &mut self.inner
        }
    }
}

unsafe impl<T> Send for ThreadRef<T> {}
unsafe impl<T> Sync for ThreadRef<T> {}
