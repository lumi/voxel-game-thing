use {
    specs::Entity,
    cgmath::{Vector3, Point3, Point2},
};

#[derive(Debug, Clone)]
pub struct MousePicker {
    pub ray_dir: Vector3<f32>,
    pub terrain_hit: Option<(Point2<i32>, Point3<usize>, Point3<f32>, Vector3<f32>)>,
    pub entity_hit: Option<(Entity, Point3<f32>)>,
}

impl Default for MousePicker {
    fn default() -> MousePicker {
        MousePicker {
            ray_dir: Vector3::new(0., 0., 1.),
            terrain_hit: None,
            entity_hit: None,
        }
    }
}
