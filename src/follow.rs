use {
    specs::prelude::*,
    cgmath::{
        prelude::*,
        Vector3,
    },
};

#[derive(Debug, Clone)]
pub struct Follow {
    pub entity: Option<Entity>,
    pub dir: Vector3<f32>,
    pub distance: f32,
}

impl Default for Follow {
    fn default() -> Follow {
        Follow {
            entity: None,
            dir: Vector3::new(1., 1., 0.2).normalize(),
            distance: 20.,
        }
    }
}
