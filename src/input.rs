use {
    cgmath::Point2,
    std::collections::HashSet
};

pub use luminance_glutin::{ElementState, MouseButton};

mod system;
pub use system::InputSystem;

#[derive(Debug, Clone)]
pub struct Cursor {
    pub screen_pos: Point2<f32>,
    pub norm_screen_pos: Point2<f32>,
}

impl Default for Cursor {
    fn default() -> Cursor {
        Cursor {
            screen_pos: Point2::new(0., 0.),
            norm_screen_pos: Point2::new(0., 0.),
        }
    }
}

#[derive(Debug, Clone)]
pub struct Keyboard {
    pub down: HashSet<u32>,
    pub pressed: Vec<u32>,
}

impl Default for Keyboard {
    fn default() -> Keyboard {
        Keyboard {
            down: HashSet::new(),
            pressed: Vec::new(),
        }
    }
}

#[derive(Debug, Clone)]
pub struct MouseEvent {
    pub state: ElementState,
    pub button: MouseButton,
}

#[derive(Debug, Clone)]
pub struct MouseScrollEvent {
    pub delta: f32,
}
