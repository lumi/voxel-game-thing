use {
    cgmath::{
        prelude::*,
        Point3,
        Vector3,
    },
};

use crate::{
    chunk::CHUNK_WORLD_SIZE,
};

#[derive(Debug, Clone, Copy)]
pub struct Ray {
    pub origin: Point3<f32>,
    pub dir: Vector3<f32>,
}

impl Ray {
    pub fn new(origin: Point3<f32>, dir: Vector3<f32>) -> Ray {
        Ray { origin, dir }
    }

    pub fn intersect_aabb(self, aabb: Aabb) -> Option<Point3<f32>> {
        let a = aabb.center - aabb.radius;
        let b = aabb.center + aabb.radius;
        let mut tmin;
        let mut tmax;
        let divx = 1. / self.dir.x;
        let divy = 1. / self.dir.y;
        let divz = 1. / self.dir.z;
        if divx >= 0. {
            tmin = (a.x - self.origin.x) * divx;
            tmax = (b.x - self.origin.x) * divx;
        }
        else {
            tmin = (b.x - self.origin.x) * divx;
            tmax = (a.x - self.origin.x) * divx;
        }
        let tymin;
        let tymax;
        if divy >= 0. {
            tymin = (a.y - self.origin.y) * divy;
            tymax = (b.y - self.origin.y) * divy;
        }
        else {
            tymin = (b.y - self.origin.y) * divy;
            tymax = (a.y - self.origin.y) * divy;
        }
        if tmin > tymax || tymin > tmax {
            return None;
        }
        if tymin > tmin {
            tmin = tymin;
        }
        if tymax < tmax {
            tmax = tymax;
        }
        let tzmin;
        let tzmax;
        if divz >= 0. {
            tzmin = (a.z - self.origin.z) * divz;
            tzmax = (b.z - self.origin.z) * divz;
        }
        else {
            tzmin = (b.z - self.origin.z) * divz;
            tzmax = (a.z - self.origin.z) * divz;
        }
        if tmin > tzmax || tzmin > tmax {
            return None;
        }
        if tzmin > tmin {
            tmin = tzmin;
        }
        if tzmax < tmax {
            tmax = tzmax;
        }
        if tmax < tmin {
            None
        }
        else if tmin > 0. {
            let hit = self.point_along(tmin);
            Some(hit)
        }
        else if tmax > 0. {
            let hit = self.point_along(tmax);
            Some(hit)
        }
        else {
            None
        }
    }

    pub fn point_along(self, dist: f32) -> Point3<f32> {
        self.origin + dist * self.dir
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Aabb {
    pub center: Point3<f32>,
    pub radius: Vector3<f32>,
}

impl Aabb {
    pub fn new(center: Point3<f32>, radius: Vector3<f32>) -> Aabb {
        Aabb { center, radius }
    }

    pub fn start(self) -> Point3<f32> {
        self.center - self.radius
    }

    pub fn end(self) -> Point3<f32> {
        self.center + self.radius
    }

    pub fn normal_at(self, pos: Point3<f32>) -> Vector3<f32> {
        let diff = (pos - self.center).div_element_wise(self.radius);
        let abs = diff.map(|x| x.abs());
        if abs.x > abs.y {
            if abs.z > abs.x {
                Vector3::unit_z() * diff.z.signum()
            }
            else {
                Vector3::unit_x() * diff.x.signum()
            }
        }
        else if abs.z > abs.y {
            Vector3::unit_z() * diff.z.signum()
        }
        else {
            Vector3::unit_y() * diff.y.signum()
        }
    }

    pub fn intersect(self, other: Aabb) -> Option<Aabb> {
        let start1 = self.start();
        let start2 = other.start();
        let end1 = self.end();
        let end2 = other.end();
        let min = Point3::new(
            start1.x.max(start2.x),
            start1.y.max(start2.y),
            start1.z.max(start2.z));
        let max = Point3::new(
            end1.x.min(end2.x),
            end1.y.min(end2.y),
            end1.z.min(end2.z));
        if min.x >= max.x || min.y >= max.y || min.z >= max.z {
            None
        }
        else {
            let diff = max - min;
            let hdiff = diff * 0.5;
            Some(Aabb::new(min + hdiff, hdiff))
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum Shape {
    Box {
        radius: Vector3<f32>,
    },
    Cylinder {
        radius: f32,
        height: f32,
    },
    Chunk,
}

impl Shape {
    pub fn to_aabb(self) -> Aabb {
        match self {
            Shape::Box { radius } => Aabb::new(Point3::origin(), radius),
            Shape::Cylinder { radius, height } => Aabb::new(Point3::origin(), Vector3::new(radius, radius, height * 0.5)),
            Shape::Chunk => Aabb::new(Point3::origin(), CHUNK_WORLD_SIZE * 0.5),
        }
    }

    pub fn to_aabb_at(self, pos: Point3<f32>) -> Aabb {
        match self {
            Shape::Box { radius } => Aabb::new(pos, radius),
            Shape::Cylinder { radius, height } => Aabb::new(pos, Vector3::new(radius, radius, height * 0.5)),
            Shape::Chunk => Aabb::new(pos, CHUNK_WORLD_SIZE * 0.5),
        }
    }
}
