use {
    specs::{
        prelude::*,
        shrev::EventChannel,
    },
    luminance::{
        context::GraphicsContext,
        render_state::RenderState,
        tess::{Mode, TessBuilder, Tess},
        face_culling::{FaceCulling, FaceCullingMode, FaceCullingOrder},
        texture::{Texture, Sampler, Flat, Dim2, GenMipmaps},
        blending::{Factor, Equation},
        pixel,
    },
    luminance_glutin::{
        GlutinSurface,
        Surface,
        Event,
        WindowDim,
        WindowEvent,
        WindowOpt,
    },
    cgmath::{
        prelude::*,
        Matrix4,
        Matrix3,
        Point3,
        Point2,
        Vector3,
    },
    image,
    std::path::Path,
};

use crate::{
    config::{WINDOW_WIDTH, WINDOW_HEIGHT},
    camera::Camera,
    chunk::{CELL_SIZE, ChunkManager, Chunk},
    thread_ref::ThreadRef,
    mouse_picker::MousePicker,
    mesh::Mesh,
    components,
};

mod shaders;
mod ui;

use shaders::Vertex;

fn load_texture<Ctx: GraphicsContext, P: AsRef<Path>>(ctx: &mut Ctx, path: P) -> Texture<Flat, Dim2, pixel::RGBA32F> {
    let img = image::open(path).unwrap().to_rgba();
    let dim = img.dimensions();
    let tex = Texture::new(ctx, [dim.0, dim.1], 0, Sampler::default()).unwrap();
    fn to_f32(p: u8) -> f32 { p as f32 / 255. }
    tex.upload(GenMipmaps::No, &img.pixels().map(|p| (to_f32(p[0]), to_f32(p[1]), to_f32(p[2]), to_f32(p[3]))).collect::<Vec<_>>()).unwrap();
    tex
}

pub struct Renderer {
    surface: GlutinSurface,
    terrain_program: shaders::TerrainProgram,
    entity_program: shaders::EntityProgram,
    ui_program: shaders::UiProgram,
    pub should_close: bool,
}

impl Renderer {
    pub fn new() -> Renderer {
        let surface = GlutinSurface::new(WindowDim::Windowed(WINDOW_WIDTH, WINDOW_HEIGHT), "game", WindowOpt::default()).unwrap();
        let terrain_program = shaders::make_terrain_program();
        let entity_program = shaders::make_entity_program();
        let ui_program = shaders::make_ui_program();
        Renderer {
            surface,
            terrain_program,
            entity_program,
            ui_program,
            should_close: false,
        }
    }

    pub fn input_pump(&mut self) -> InputPumpSystem {
        InputPumpSystem { renderer: self }
    }
}

impl<'a> System<'a> for Renderer {
    type SystemData = (
        Write<'a, Camera>,
        Write<'a, ChunkManager>,
        Read<'a, MousePicker>,
        ReadStorage<'a, components::Pos>,
        WriteStorage<'a, components::HasMesh>,
        ReadStorage<'a, components::Solid>,
    );

    fn run(&mut self, (mut camera, mut cm, mp, pos_store, mut hasmesh_store, solid_store): Self::SystemData) {
        let back_buffer = self.surface.back_buffer().unwrap();

        for chunk in cm.chunks.values_mut() {
            if chunk.tess.is_none() {
                let tess = tess_terrain(&mut self.surface, chunk);
                chunk.tess = Some(ThreadRef::new(tess));
            }
        }

        let selection_mesh = Mesh::new_box(CELL_SIZE * 0.55);

        let mut et = EntityTesselator { color: [90, 90, 240] };
        let selection_tess = et.tesselate(&mut self.surface, &selection_mesh);

        for (solid, hasmesh) in (&solid_store, &mut hasmesh_store).join() {
            if hasmesh.tess.is_none() {
                let aabb = solid.shape.to_aabb();
                let mesh = Mesh::new_box(aabb.radius);
                et.color = hasmesh.color;
                let tess = et.tesselate(&mut self.surface, &mesh);
                hasmesh.tess = Some(ThreadRef::new(tess));
            }
        }

        let terrain_program = &self.terrain_program;
        let entity_program = &self.entity_program;
        let ui_program = &self.ui_program;

        let size = self.surface.size();
        let size_f32 = [size[0] as f32, size[1] as f32];
        camera.screen_size = size_f32.into();

        let light_dir = Vector3::new(1., 1., 1.5).normalize();

        et.color = [0, 0, 255];
        let cursor_entity_mesh = Mesh::new_sphere(Point3::origin(), 0.3, 5);
        let cursor_entity_tess = et.tesselate(&mut self.surface, &cursor_entity_mesh);

        let mut canvas = ui::Canvas::new(size_f32.into());
        ui::draw_ui(&mut canvas);
        let mut ui_vertices = Vec::new();
        for command in canvas.into_commands() {
            command.write_to(&mut ui_vertices);
        }
        let ui_tess = TessBuilder::new(&mut self.surface)
            .add_vertices(&ui_vertices)
            .set_mode(Mode::Triangle)
            .build()
            .unwrap();

        self.surface
            .pipeline_builder()
            .pipeline(&back_buffer, [0., 0., 0., 0.], |_pipeline, mut shd_gate| {
                let rs = RenderState::default()
                    .set_face_culling(FaceCulling::new(FaceCullingOrder::CCW, FaceCullingMode::Back));
                shd_gate.shade(terrain_program, |iface, mut rdr_gate| {
                    rdr_gate.render(rs, |mut tess_gate| {
                        iface.view_matrix.update(camera.view_matrix().into());
                        iface.perspective_matrix.update(camera.perspective_matrix().into());
                        iface.light_dir.update(light_dir.into());
                        for chunk in cm.chunks.values() {
                            iface.model_matrix.update(Matrix4::from_translation(chunk.start.to_vec()).into());
                            if let Some(tess) = &chunk.tess {
                                tess_gate.render(&**tess);
                            }
                        }
                    });
                });
                shd_gate.shade(entity_program, |iface, mut rdr_gate| {
                    rdr_gate.render(rs, |mut tess_gate| {
                        iface.view_matrix.update(camera.view_matrix().into());
                        iface.perspective_matrix.update(camera.perspective_matrix().into());
                        iface.light_dir.update(light_dir.into());
                        for (pos, hasmesh) in (&pos_store, &hasmesh_store).join() {
                            if let Some(tess) = &hasmesh.tess {
                                iface.model_matrix.update(Matrix4::from_translation(pos.pos.to_vec()).into());
                                tess_gate.render(&**tess);
                            }
                        }
                        if let Some((_chunk, _cell_pos, hit_pos, _normal)) = mp.terrain_hit {
                            //let t = cell_center(cell_pos.cast::<i32>().unwrap()) + normal.mul_element_wise(CELL_SIZE);
                            //iface.model_matrix.update(Matrix4::from_translation(t.to_vec()).into());
                            //tess_gate.render(&selection_tess);
                            iface.model_matrix.update(Matrix4::from_translation(hit_pos.to_vec()).into());
                            tess_gate.render(&cursor_entity_tess);
                        }
                    });
                });
                let ui_rs = RenderState::default()
                    .set_depth_test(None)
                    .set_blending((Equation::Additive, Factor::SrcAlpha, Factor::SrcAlphaComplement));
                shd_gate.shade(ui_program, |iface, mut rdr_gate| {
                    iface.window_size.update(size_f32);
                    rdr_gate.render(ui_rs, |mut tess_gate| {
                        iface.transform_matrix.update(Matrix3::identity().into());
                        tess_gate.render(&ui_tess);
                    });
                });
            });

        self.surface.swap_buffers();
    }
}

pub struct InputPumpSystem<'a> {
    renderer: &'a mut Renderer,
}

impl<'a, 'b> System<'a> for InputPumpSystem<'b> {
    type SystemData = (
        Write<'a, EventChannel<Event>>,
    );

    fn run(&mut self, (mut event_channel,): Self::SystemData) {
        for event in self.renderer.surface.poll_events() {
            match &event {
                Event::WindowEvent { event, .. } => match event {
                    WindowEvent::CloseRequested => { self.renderer.should_close = true; },
                    _ => (),
                },
                _ => (),
            }
            event_channel.single_write(event);
        }
    }
}

trait Tesselator {
    fn tesselate<Ctx: GraphicsContext>(&self, ctx: &mut Ctx, mesh: &Mesh) -> Tess;
}

pub struct EntityTesselator { pub color: [u8; 3], }

impl Tesselator for EntityTesselator {
    fn tesselate<Ctx: GraphicsContext>(&self, ctx: &mut Ctx, mesh: &Mesh) -> Tess {
        let vertices: Vec<_> = mesh.vertices.iter().map(|v| Vertex::make(v.pos, v.normal, v.uv, self.color)).collect();
        TessBuilder::new(ctx)
            .add_vertices(&vertices)
            .set_mode(Mode::Triangle)
            .build()
            .unwrap()
    }
}

pub struct TerrainTesselator { pub color: [u8; 3], }

impl Tesselator for TerrainTesselator {
    fn tesselate<Ctx: GraphicsContext>(&self, ctx: &mut Ctx, mesh: &Mesh) -> Tess {
        let vertices: Vec<_> = mesh.vertices.iter().map(|v| Vertex::make(v.pos, v.normal, v.uv, self.color)).collect();
        TessBuilder::new(ctx)
            .add_vertices(&vertices)
            .set_mode(Mode::Triangle)
            .build()
            .unwrap()
    }
}

fn tess_terrain<Ctx: GraphicsContext>(ctx: &mut Ctx, chunk: &Chunk) -> Tess {
    let mut vertices: Vec<Vertex> = Vec::new();
    for ((ix, iy, iz), &cell) in chunk.data.indexed_iter() {
        if cell > 0 {
            let w_start = Point3::new(ix as f32 * CELL_SIZE.x, iy as f32 * CELL_SIZE.y, iz as f32 * CELL_SIZE.z);
            let col = [0, 255, 0];
            let (csx, csy, csz) = (CELL_SIZE.x, CELL_SIZE.y, CELL_SIZE.z);
            // x+
            if chunk.data.get((ix + 1, iy, iz)).and_then(|&c| if c > 0 { Some(()) } else { None }).is_none() {
                let normal = Vector3::new(1., 0., 0.);
                vertices.extend(&[
                    Vertex::make(w_start + Vector3::new(csx, 0. , 0. ), normal, Point2::new(0., 0.), col),
                    Vertex::make(w_start + Vector3::new(csx, csy, 0. ), normal, Point2::new(1., 0.), col),
                    Vertex::make(w_start + Vector3::new(csx, csy, csz), normal, Point2::new(1., 1.), col),
                    Vertex::make(w_start + Vector3::new(csx, csy, csz), normal, Point2::new(1., 1.), col),
                    Vertex::make(w_start + Vector3::new(csx, 0. , csz), normal, Point2::new(0., 1.), col),
                    Vertex::make(w_start + Vector3::new(csx, 0. , 0. ), normal, Point2::new(0., 0.), col),
                ]);
            }
            // x-
            if ix > 1 && chunk.data.get((ix - 1, iy, iz)).and_then(|&c| if c > 0 { Some(()) } else { None }).is_none() {
                let normal = Vector3::new(-1., 0., 0.);
                vertices.extend(&[
                    Vertex::make(w_start + Vector3::new(0. , 0. , 0. ), normal, Point2::new(0., 0.), col),
                    Vertex::make(w_start + Vector3::new(0. , csy, csz), normal, Point2::new(1., 1.), col),
                    Vertex::make(w_start + Vector3::new(0. , csy, 0. ), normal, Point2::new(1., 0.), col),
                    Vertex::make(w_start + Vector3::new(0. , 0. , csz), normal, Point2::new(0., 1.), col),
                    Vertex::make(w_start + Vector3::new(0. , csy, csz), normal, Point2::new(1., 1.), col),
                    Vertex::make(w_start + Vector3::new(0. , 0. , 0. ), normal, Point2::new(0., 0.), col),
                ]);
            }
            // y+
            if chunk.data.get((ix, iy + 1, iz)).and_then(|&c| if c > 0 { Some(()) } else { None }).is_none() {
                let normal = Vector3::new(0., 1., 0.);
                vertices.extend(&[
                    Vertex::make(w_start + Vector3::new(0. , csy, 0. ), normal, Point2::new(0., 0.), col),
                    Vertex::make(w_start + Vector3::new(csx, csy, csz), normal, Point2::new(1., 1.), col),
                    Vertex::make(w_start + Vector3::new(csx, csy, 0. ), normal, Point2::new(1., 0.), col),
                    Vertex::make(w_start + Vector3::new(0. , csy, csz), normal, Point2::new(0., 1.), col),
                    Vertex::make(w_start + Vector3::new(csx, csy, csz), normal, Point2::new(1., 1.), col),
                    Vertex::make(w_start + Vector3::new(0. , csy, 0. ), normal, Point2::new(0., 0.), col),
                ]);
            }
            // y-
            if iy > 1 && chunk.data.get((ix, iy - 1, iz)).and_then(|&c| if c > 0 { Some(()) } else { None }).is_none() {
                let normal = Vector3::new(0., -1., 0.);
                vertices.extend(&[
                    Vertex::make(w_start + Vector3::new(0. , 0. , 0. ), normal, Point2::new(0., 0.), col),
                    Vertex::make(w_start + Vector3::new(csx, 0. , 0. ), normal, Point2::new(1., 0.), col),
                    Vertex::make(w_start + Vector3::new(csx, 0. , csz), normal, Point2::new(1., 1.), col),
                    Vertex::make(w_start + Vector3::new(csx, 0. , csz), normal, Point2::new(1., 1.), col),
                    Vertex::make(w_start + Vector3::new(0. , 0. , csz), normal, Point2::new(0., 1.), col),
                    Vertex::make(w_start + Vector3::new(0. , 0. , 0. ), normal, Point2::new(0., 0.), col),
                ]);
            }
            // z+
            if chunk.data.get((ix, iy, iz + 1)).and_then(|&c| if c > 0 { Some(()) } else { None }).is_none() {
                let normal = Vector3::new(0., 0., 1.);
                vertices.extend(&[
                    Vertex::make(w_start + Vector3::new(0. , 0. , csz), normal, Point2::new(0., 0.), col),
                    Vertex::make(w_start + Vector3::new(csx, 0. , csz), normal, Point2::new(1., 0.), col),
                    Vertex::make(w_start + Vector3::new(csx, csy, csz), normal, Point2::new(1., 1.), col),
                    Vertex::make(w_start + Vector3::new(csx, csy, csz), normal, Point2::new(1., 1.), col),
                    Vertex::make(w_start + Vector3::new(0. , csy, csz), normal, Point2::new(0., 1.), col),
                    Vertex::make(w_start + Vector3::new(0. , 0. , csz), normal, Point2::new(0., 0.), col),

                ]);
            }
            // z-
            if iz > 1 && chunk.data.get((ix, iy, iz - 1)).and_then(|&c| if c > 0 { Some(()) } else { None }).is_none() {
                let normal = Vector3::new(0., 0., -1.);
                vertices.extend(&[
                    Vertex::make(w_start + Vector3::new(0. , 0. , 0.), normal, Point2::new(0., 0.), col),
                    Vertex::make(w_start + Vector3::new(csx, csy, 0.), normal, Point2::new(1., 1.), col),
                    Vertex::make(w_start + Vector3::new(csx, 0. , 0.), normal, Point2::new(1., 0.), col),
                    Vertex::make(w_start + Vector3::new(0. , csy, 0.), normal, Point2::new(0., 1.), col),
                    Vertex::make(w_start + Vector3::new(csx, csy, 0.), normal, Point2::new(1., 1.), col),
                    Vertex::make(w_start + Vector3::new(0. , 0. , 0.), normal, Point2::new(0., 0.), col),
                ]);
            }
        }
    }
    TessBuilder::new(ctx)
        .add_vertices(&vertices)
        .set_mode(Mode::Triangle)
        .build()
        .unwrap()
}
