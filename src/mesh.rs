use {
    cgmath::{Point3, Point2, Vector3},
};

#[derive(Debug, Clone, Copy)]
pub struct Vertex {
    pub pos: Point3<f32>,
    pub normal: Vector3<f32>,
    pub uv: Point2<f32>,
}

impl Vertex {
    pub fn new(pos: Point3<f32>, normal: Vector3<f32>, uv: Point2<f32>) -> Vertex {
        Vertex { pos, normal, uv }
    }
}

#[derive(Debug, Clone)]
pub struct Mesh {
    pub vertices: Vec<Vertex>,
}

impl Mesh {
    pub fn new_box(radius: Vector3<f32>) -> Mesh {
        let (rx, ry, rz) = (radius.x, radius.y, radius.z);
        let normal_xp = Vector3::new( 1.,  0.,  0.);
        let normal_xn = Vector3::new(-1.,  0.,  0.);
        let normal_yp = Vector3::new( 0.,  1.,  0.);
        let normal_yn = Vector3::new( 0., -1.,  0.);
        let normal_zp = Vector3::new( 0.,  0.,  1.);
        let normal_zn = Vector3::new( 0.,  0., -1.);
        let vertices = vec![
            // x+
            Vertex::new(Point3::new( rx, -ry, -rz), normal_xp, Point2::new(0., 0.)),
            Vertex::new(Point3::new( rx,  ry, -rz), normal_xp, Point2::new(1., 0.)),
            Vertex::new(Point3::new( rx,  ry,  rz), normal_xp, Point2::new(1., 1.)),
            Vertex::new(Point3::new( rx,  ry,  rz), normal_xp, Point2::new(1., 1.)),
            Vertex::new(Point3::new( rx, -ry,  rz), normal_xp, Point2::new(0., 1.)),
            Vertex::new(Point3::new( rx, -ry, -rz), normal_xp, Point2::new(0., 0.)),
            // x-
            Vertex::new(Point3::new(-rx, -ry, -rz), normal_xn, Point2::new(0., 0.)),
            Vertex::new(Point3::new(-rx,  ry,  rz), normal_xn, Point2::new(1., 1.)),
            Vertex::new(Point3::new(-rx,  ry, -rz), normal_xn, Point2::new(1., 0.)),
            Vertex::new(Point3::new(-rx, -ry,  rz), normal_xn, Point2::new(0., 1.)),
            Vertex::new(Point3::new(-rx,  ry,  rz), normal_xn, Point2::new(1., 1.)),
            Vertex::new(Point3::new(-rx, -ry, -rz), normal_xn, Point2::new(0., 0.)),
            // y+
            Vertex::new(Point3::new(-rx,  ry, -rz), normal_yp, Point2::new(0., 0.)),
            Vertex::new(Point3::new( rx,  ry,  rz), normal_yp, Point2::new(1., 1.)),
            Vertex::new(Point3::new( rx,  ry, -rz), normal_yp, Point2::new(1., 0.)),
            Vertex::new(Point3::new(-rx,  ry,  rz), normal_yp, Point2::new(0., 1.)),
            Vertex::new(Point3::new( rx,  ry,  rz), normal_yp, Point2::new(1., 1.)),
            Vertex::new(Point3::new(-rx,  ry, -rz), normal_yp, Point2::new(0., 0.)),
            // y-
            Vertex::new(Point3::new(-rx, -ry, -rz), normal_yn, Point2::new(0., 0.)),
            Vertex::new(Point3::new( rx, -ry, -rz), normal_yn, Point2::new(1., 0.)),
            Vertex::new(Point3::new( rx, -ry,  rz), normal_yn, Point2::new(1., 1.)),
            Vertex::new(Point3::new( rx, -ry,  rz), normal_yn, Point2::new(1., 1.)),
            Vertex::new(Point3::new(-rx, -ry,  rz), normal_yn, Point2::new(0., 1.)),
            Vertex::new(Point3::new(-rx, -ry, -rz), normal_yn, Point2::new(0., 0.)),
            // z+
            Vertex::new(Point3::new(-rx, -ry,  rz), normal_zp, Point2::new(0., 0.)),
            Vertex::new(Point3::new( rx, -ry,  rz), normal_zp, Point2::new(1., 0.)),
            Vertex::new(Point3::new( rx,  ry,  rz), normal_zp, Point2::new(1., 1.)),
            Vertex::new(Point3::new( rx,  ry,  rz), normal_zp, Point2::new(1., 1.)),
            Vertex::new(Point3::new(-rx,  ry,  rz), normal_zp, Point2::new(0., 1.)),
            Vertex::new(Point3::new(-rx, -ry,  rz), normal_zp, Point2::new(0., 0.)),
            // z-
            Vertex::new(Point3::new(-rx, -ry, -rz), normal_zn, Point2::new(0., 0.)),
            Vertex::new(Point3::new( rx,  ry, -rz), normal_zn, Point2::new(1., 1.)),
            Vertex::new(Point3::new( rx, -ry, -rz), normal_zn, Point2::new(1., 0.)),
            Vertex::new(Point3::new(-rx,  ry, -rz), normal_zn, Point2::new(0., 1.)),
            Vertex::new(Point3::new( rx,  ry, -rz), normal_zn, Point2::new(1., 1.)),
            Vertex::new(Point3::new(-rx, -ry, -rz), normal_zn, Point2::new(0., 0.)),
        ];
        Mesh { vertices }
    }

    pub fn new_sphere(center: Point3<f32>, radius: f32, resolution: usize) -> Mesh {
        let res = resolution as f32;
        let mut vertices = Vec::new();
        let mut phi = 0f32;
        let mut theta = 0f32;
        let delta_theta = std::f32::consts::PI / (res + 2.);
        let delta_phi = 2. * std::f32::consts::PI / res;
        for _ring_idx in 0..resolution + 3 {
            let theta_next = theta + delta_theta;
            let z0 = radius * theta.cos();
            let z1 = radius * theta_next.cos();
            for _point_idx in 0..resolution + 1 {
                let phi_next = phi + delta_phi;
                let x00 = radius * theta.sin() * phi.cos();
                let y00 = radius * theta.sin() * phi.sin();
                let v00 = Vector3::new(x00, y00, z0);
                let x01 = radius * theta.sin() * phi_next.cos();
                let y01 = radius * theta.sin() * phi_next.sin();
                let v01 = Vector3::new(x01, y01, z0);
                let x10 = radius * theta_next.sin() * phi.cos();
                let y10 = radius * theta_next.sin() * phi.sin();
                let v10 = Vector3::new(x10, y10, z1);
                let x11 = radius * theta_next.sin() * phi_next.cos();
                let y11 = radius * theta_next.sin() * phi_next.sin();
                let v11 = Vector3::new(x11, y11, z1);
                let theta_normal = theta + delta_theta * 0.5;
                let phi_normal = phi + delta_phi * 0.5;
                let normal = Vector3::new(theta_normal.sin() * phi_normal.cos(), theta_normal.sin() * phi_normal.sin(), theta_normal.cos());
                vertices.extend_from_slice(&[
                    Vertex::new(center + v00, normal, Point2::new(0., 0.)),
                    Vertex::new(center + v10, normal, Point2::new(1., 0.)),
                    Vertex::new(center + v11, normal, Point2::new(1., 1.)),
                    Vertex::new(center + v11, normal, Point2::new(1., 1.)),
                    Vertex::new(center + v01, normal, Point2::new(0., 1.)),
                    Vertex::new(center + v00, normal, Point2::new(0., 0.)),
                ]);
                phi += delta_phi;
            }
            theta += delta_theta;
        }
        Mesh { vertices }
    }
}
