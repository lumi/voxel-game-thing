use {
    specs::{
        prelude::*,
    },
};

use crate::{
    prim::Aabb,
};

#[derive(Debug, Clone, Default)]
pub struct CollisionManager {
    pub static_colliders: Vec<(Entity, Aabb)>,
    pub dynamic_colliders: Vec<(Entity, Aabb)>,
}
