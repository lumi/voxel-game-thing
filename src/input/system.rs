use {
    cgmath::{prelude::*, Point2, Vector2},
    specs::{
        prelude::*,
        shrev::EventChannel,
    },
    luminance_glutin as glutin,
    luminance_glutin::{
        WindowEvent,
        ElementState,
    },
};

use super::{Cursor, Keyboard, MouseEvent};

use crate::{
    config::{WINDOW_HEIGHT, WINDOW_WIDTH},
    camera::Camera,
};

pub struct InputSystem {
    events_reader_id: ReaderId<glutin::Event>,
}

impl InputSystem {
    pub fn new(events_reader_id: ReaderId<glutin::Event>) -> InputSystem {
        InputSystem { events_reader_id }
    }
}

impl<'a> System<'a> for InputSystem {
    type SystemData = (
        Write<'a, Cursor>,
        Write<'a, Keyboard>,
        Read<'a, Camera>,
        Read<'a, EventChannel<glutin::Event>>,
        Write<'a, EventChannel<MouseEvent>>,
    );

    fn run(&mut self, (mut cs, mut kb, cam, glutin_events, mut mouse_events): Self::SystemData) {
        kb.pressed.clear();
        for evt in glutin_events.read(&mut self.events_reader_id) {
            match evt {
                glutin::Event::WindowEvent { event, .. } => match event {
                    WindowEvent::CursorMoved { position, .. } => {
                        cs.screen_pos = Point2::new(position.x as f32, position.y as f32);
                    }
                    WindowEvent::MouseInput { state, button, .. } => {
                        mouse_events.single_write(MouseEvent { state: state.clone(), button: button.clone() });
                    },
                    WindowEvent::KeyboardInput { input, .. } => match input.state {
                        ElementState::Pressed => {
                            kb.pressed.push(input.scancode);
                            kb.down.insert(input.scancode);
                        }
                        ElementState::Released => {
                            kb.down.remove(&input.scancode);
                        }
                    },
                    _ => (),
                },
                _ => (),
            }
        }
        let norm_screen_pos = Point2::from_vec(
            cs.screen_pos.to_vec().mul_element_wise(Vector2::new(
                2. / cam.screen_size.x,
                -2. / cam.screen_size.y,
            )) - Vector2::new(1., -1.),
        );
        cs.norm_screen_pos = norm_screen_pos;
    }
}
