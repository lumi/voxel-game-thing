use {
    specs::{
        prelude::*,
        shrev::EventChannel,
    },
    cgmath::{
        Point3,
        Vector3,
    },
    std::collections::VecDeque,
};

mod agent;
mod camera;
mod chunk;
mod collision;
mod components;
mod config;
mod follow;
mod input;
mod mouse_picker;
mod prim;
mod renderer;
mod systems;
mod thread_ref;
mod math;
mod mesh;

fn make_world() -> World {
    let mut world = World::new();
    world.register::<components::Pos>();
    world.register::<components::Vel>();
    world.register::<components::HasMesh>();
    world.register::<components::Agent>();
    world.register::<components::Mobile>();
    world.register::<components::Solid>();
    world.register::<components::IsChunk>();
    world.insert(follow::Follow::default());
    world.insert(camera::Camera::default());
    world.insert(chunk::ChunkManager::default());
    world.insert(collision::CollisionManager::default());
    world.insert(input::Cursor::default());
    world.insert(input::Keyboard::default());
    world.insert(mouse_picker::MousePicker::default());
    world.insert(EventChannel::<luminance_glutin::Event>::new());
    world.insert(EventChannel::<input::MouseEvent>::new());
    let player = world.create_entity()
        .with(components::Pos { pos: Point3::new(0., 0., 1.5) })
        .with(components::Vel { vel: Vector3::new(0., 0., 0.) })
        .with(components::HasMesh { tess: None, color: [240, 99, 240] })
        .with(components::Agent { orders: VecDeque::new() })
        .with(components::Mobile { max_speed: 0.1 })
        .with(components::Solid { shape: prim::Shape::Cylinder { height: 2., radius: 0.5 } })
        .build();
    let thing = world.create_entity()
        .with(components::Pos { pos: Point3::new(0., 10., 1.) })
        .with(components::Vel { vel: Vector3::new(0., 0., 0.) })
        .with(components::HasMesh { tess: None, color: [240, 90, 90] })
        .with(components::Solid { shape: prim::Shape::Cylinder { height: 1.5, radius: 0.3 } })
        .build();
    world.fetch_mut::<follow::Follow>().entity = Some(player);
    world
}

fn main() {
    let mut world = make_world();
    let mut renderer = renderer::Renderer::new();
    let glutin_events_reader = world.fetch_mut::<EventChannel<luminance_glutin::Event>>().register_reader();
    let mouse_events_reader_1 = world.fetch_mut::<EventChannel<input::MouseEvent>>().register_reader();
    let mouse_events_reader_2 = world.fetch_mut::<EventChannel<input::MouseEvent>>().register_reader();
    let mut dispatcher = DispatcherBuilder::new()
        .with(input::InputSystem::new(glutin_events_reader), "input", &[])
        .with(systems::ChunkLoaderSystem, "chunk_loader", &[])
        .with(systems::PhysicsSystem, "physics", &[ "chunk_loader" ])
        .with(systems::CollisionSystem, "collision", &[ "physics", "chunk_loader" ])
        .with(systems::FollowSystem::new(mouse_events_reader_1), "follow", &[ "input", "physics", "collision" ])
        .with(systems::MousePickerSystem, "mouse_picker", &[ "input", "follow", "chunk_loader", "collision" ])
//        .with(systems::BlockPlacerSystem::new(mouse_events_reader_2), "block_placer", &[ "input", "chunk_loader", "mouse_picker" ])
        .with(systems::PlayerControlSystem::new(mouse_events_reader_2), "player_control", &[ "mouse_picker", "collision" ])
        .with(systems::AgentSystem, "agent", &[ "physics", "chunk_loader", "player_control", "collision" ])
        .build();
    'app: loop {
        renderer.input_pump().run_now(&mut world);
        dispatcher.dispatch(&mut world);
        world.maintain();
        renderer.run_now(&mut world);
        if renderer.should_close {
            break 'app;
        }
    }
}
