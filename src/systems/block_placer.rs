use {
    specs::{
        prelude::*,
        shrev::{
            EventChannel,
            ReaderId,
        },
    },
};

use crate::{
    mouse_picker::MousePicker,
    input::{MouseEvent, MouseButton, ElementState},
    chunk::ChunkManager,
};

pub struct BlockPlacerSystem {
    mouse_events_reader_id: ReaderId<MouseEvent>,
}

impl BlockPlacerSystem {
    pub fn new(mouse_events_reader_id: ReaderId<MouseEvent>) -> BlockPlacerSystem {
        BlockPlacerSystem {
            mouse_events_reader_id,
        }
    }
}

impl<'a> System<'a> for BlockPlacerSystem {
    type SystemData = (
        Read<'a, MousePicker>,
        Read<'a, EventChannel<MouseEvent>>,
        Write<'a, ChunkManager>,
    );

    fn run(&mut self, (mp, mouse_events, mut cm): Self::SystemData) {
        for mouse_event in mouse_events.read(&mut self.mouse_events_reader_id) {
            if mouse_event.state == ElementState::Pressed {
                match mouse_event.button {
                    MouseButton::Left => {
                        if let Some((chunk, pos, _hit_pos, normal)) = mp.terrain_hit {
                            let t = (pos.cast::<i32>().unwrap() + normal.cast::<i32>().unwrap()).cast::<usize>().unwrap();
                            if let Some(chunk) = cm.get_mut(chunk) {
                                chunk.data[(t.x, t.y, t.z)] = 1;
                            }
                        }
                    },
                    _ => (),
                }
            }
        }
    }
}
