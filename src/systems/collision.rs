use {
    specs::{
        prelude::*,
    },
};

use crate::{
    chunk::ChunkManager,
    collision::CollisionManager,
    components::{Pos, Vel, Solid},
};

pub struct CollisionSystem;

impl<'a> System<'a> for CollisionSystem {
    type SystemData = (
        Entities<'a>,
        Read<'a, ChunkManager>,
        Write<'a, CollisionManager>,
        WriteStorage<'a, Pos>,
        WriteStorage<'a, Vel>,
        ReadStorage<'a, Solid>,
    );

    fn run(&mut self, (entities, cm, mut clm, mut pos_store, mut vel_store, solid_store): Self::SystemData) {
        clm.static_colliders.clear();
        clm.dynamic_colliders.clear();
        for (entity, pos, vel, solid) in (&entities, &pos_store, &vel_store, &solid_store).join() {
            clm.dynamic_colliders.push((entity, solid.shape.to_aabb_at(pos.pos)));
        }
        for (entity, pos, (), solid) in (&entities, &pos_store, !&vel_store, &solid_store).join() {
            clm.static_colliders.push((entity, solid.shape.to_aabb_at(pos.pos)));
        }
        let mut pairs = Vec::new();
        for dc in &clm.dynamic_colliders {
            for sc in &clm.static_colliders {
                if sc.1.intersect(dc.1).is_some() {
                    pairs.push((sc.0, dc.0));
                }
            }
        }
        eprintln!("COLLISIONS:");
        for p in pairs {
            eprintln!("{:?} -- {:?}", p.0, p.1);
        }
    }
}
