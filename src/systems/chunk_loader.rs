use {
    specs::prelude::*,
};

use crate::{
    follow::Follow,
    chunk::{ChunkManager, CHUNK_VIEW_RADIUS, chunk_center, CHUNK_WORLD_SIZE},
    components::{Pos, IsChunk, Solid},
    prim::{Shape, Aabb},
};

pub struct ChunkLoaderSystem;

impl<'a> System<'a> for ChunkLoaderSystem {
    type SystemData = (
        Entities<'a>,
        Read<'a, LazyUpdate>,
        Write<'a, ChunkManager>,
        Read<'a, Follow>,
        WriteStorage<'a, Pos>,
        WriteStorage<'a, IsChunk>,
        WriteStorage<'a, Solid>,
    );

    fn run(&mut self, (entities, lazy, mut cm, follow, pos_store, ischunk_store, solid_store): Self::SystemData) {
        if let Some(entity) = follow.entity {
            if let Some(pos) = pos_store.get(entity) {
                let mut load_cb = |cp| {
                    let center = chunk_center(cp);
                    lazy.create_entity(&entities)
                        .with(Pos { pos: center })
                        .with(IsChunk { pos: cp })
                        .with(Solid { shape: Shape::Chunk })
                        .build()
                };
                let mut unload_cb = |ent| {
                    lazy.exec_mut(move |world| world.delete_entity(ent).unwrap());
                };
                cm.maintain_radius([pos.pos.x, pos.pos.y].into(), CHUNK_VIEW_RADIUS, load_cb, unload_cb);
            }
        }
    }
}
