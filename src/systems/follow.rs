use {
    specs::{
        prelude::*,
        shrev::{
            EventChannel,
            ReaderId,
        },
    },
    cgmath::{
        prelude::*,
        Vector3,
        Point2,
        Matrix3,
        Rad,
    },
};

use crate::{
    follow::Follow,
    camera::Camera,
    components::Pos,
    input::{MouseEvent, ElementState, MouseButton, Cursor},
};

pub struct FollowSystem {
    mouse_events_reader_id: ReaderId<MouseEvent>,
    drag_state: Option<(Point2<f32>, Vector3<f32>)>,
}

impl FollowSystem {
    pub fn new(mouse_events_reader_id: ReaderId<MouseEvent>) -> FollowSystem {
        FollowSystem {
            mouse_events_reader_id,
            drag_state: None,
        }
    }
}

impl<'a> System<'a> for FollowSystem {
    type SystemData = (
        Write<'a, Camera>,
        Write<'a, Follow>,
        Read<'a, Cursor>,
        Read<'a, EventChannel<MouseEvent>>,
        ReadStorage<'a, Pos>,
    );

    fn run(&mut self, (mut camera, mut follow, cursor, mouse_events, pos_store): Self::SystemData) {
        if let Some(entity) = follow.entity {
            for mouse_event in mouse_events.read(&mut self.mouse_events_reader_id) {
                match (mouse_event.button, mouse_event.state) {
                    (MouseButton::Middle, ElementState::Pressed) => {
                        self.drag_state = Some((cursor.norm_screen_pos, follow.dir));
                    },
                    (MouseButton::Middle, ElementState::Released) => {
                        self.drag_state = None;
                    },
                    _ => (),
                }
            }
            if let Some((old_sp, old_dir)) = self.drag_state {
                let sp = cursor.norm_screen_pos;
                let diff = sp - old_sp;
                let cross = old_dir.cross(Vector3::unit_z());
                follow.dir = Matrix3::from_angle_z(Rad(diff.x * 2. * std::f32::consts::PI))
                           * Matrix3::from_axis_angle(cross.normalize(), Rad(diff.y * std::f32::consts::PI))
                           * old_dir;
            }
            if let Some(pos) = pos_store.get(entity) {
                let offset = follow.dir * follow.distance;
                camera.eye = pos.pos + offset;
                camera.up = Vector3::new(0., 0., 1.);
                camera.dir = -follow.dir;
            }
        }
    }
}
