use {
    specs::{
        prelude::*,
        shrev::{EventChannel, ReaderId},
    },
    cgmath::{
        Vector3,
    },
};

use crate::{
    follow::Follow,
    components::Agent,
    agent::Order,
    input::{MouseEvent, MouseButton, ElementState},
    mouse_picker::MousePicker,
};

pub struct PlayerControlSystem {
    mouse_event_reader_id: ReaderId<MouseEvent>,
}

impl PlayerControlSystem {
    pub fn new(mouse_event_reader_id: ReaderId<MouseEvent>) -> PlayerControlSystem {
        PlayerControlSystem { mouse_event_reader_id }
    }
}

impl<'a> System<'a> for PlayerControlSystem {
    type SystemData = (
        Read<'a, Follow>,
        Read<'a, MousePicker>,
        Read<'a, EventChannel<MouseEvent>>,
        WriteStorage<'a, Agent>,
    );

    fn run(&mut self, (follow, mp, mouse_events, mut agent_store): Self::SystemData) {
        for mouse_event in mouse_events.read(&mut self.mouse_event_reader_id) {
            match (mouse_event.button, mouse_event.state) {
                (MouseButton::Left, ElementState::Pressed) => {
                    if let Some(entity) = follow.entity {
                        if let (Some(agent), Some((_, _, hit_pos, _))) = (agent_store.get_mut(entity), mp.terrain_hit) {
                            agent.orders.clear();
                            agent.orders.push_back(Order::MoveTo(hit_pos + Vector3::new(0., 0., 1.25)));
                        }
                    }
                },
                _ => (),
            }
        }
    }
}
