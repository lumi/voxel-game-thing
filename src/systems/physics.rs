use {
    specs::prelude::*,
};

use crate::{
    components::{Pos, Vel},
};


pub struct PhysicsSystem;

impl<'a> System<'a> for PhysicsSystem {
    type SystemData = (
        WriteStorage<'a, Pos>,
        ReadStorage<'a, Vel>,
    );

    fn run(&mut self, (mut pos_store, vel_store): Self::SystemData) {
        for (pos, vel) in (&mut pos_store, &vel_store).join() {
            pos.pos += vel.vel;
        }
    }
}
