use {
    specs::prelude::*,
    cgmath::Vector4,
};

use crate::{
    mouse_picker::MousePicker,
    camera::Camera,
    chunk::ChunkManager,
    input::Cursor,
    prim::Ray,
};

pub struct MousePickerSystem;

impl<'a> System<'a> for MousePickerSystem {
    type SystemData = (
        Write<'a, MousePicker>,
        Read<'a, Camera>,
        Read<'a, ChunkManager>,
        Read<'a, Cursor>,
    );

    fn run(&mut self, (mut mp, cam, cm, cursor): Self::SystemData) {
        let screen_pos = cursor.norm_screen_pos;
        let clip_pos = Vector4::new(screen_pos.x, screen_pos.y, -1., 1.);
        let ipm = cam.inverse_perspective_matrix();
        let eye_ = ipm * clip_pos;
        let eye = Vector4::new(eye_.x, eye_.y, -1., 0.);
        let ivm = cam.inverse_view_matrix();
        let ray_dir = ivm * eye;
        mp.ray_dir = ray_dir.truncate();
        mp.terrain_hit = cm.cast_ray(Ray::new(cam.eye, mp.ray_dir));
    }
}
