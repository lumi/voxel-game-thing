use {
    specs::prelude::*,
    cgmath::{
        prelude::*,
        Vector3,
    },
};

use crate::{
    components::{Pos, Vel, Agent, Mobile},
    agent::Order,
};

pub struct AgentSystem;

impl<'a> System<'a> for AgentSystem {
    type SystemData = (
        Entities<'a>,
        ReadStorage<'a, Pos>,
        WriteStorage<'a, Vel>,
        WriteStorage<'a, Agent>,
        ReadStorage<'a, Mobile>,
    );

    fn run(&mut self, (entities, pos_store, mut vel_store, mut agent_store, mobile_store): Self::SystemData) {
        for (entity, pos, vel, agent) in (&entities, &pos_store, &mut vel_store, &mut agent_store).join() {
            if let Some(order) = agent.orders.pop_front() {
                match order {
                    Order::MoveTo(tg_pos) => {
                        if let Some(mobile) = mobile_store.get(entity) {
                            let diff = tg_pos - pos.pos;
                            if diff.magnitude() < 0.3 {
                                vel.vel = Vector3::new(0., 0., 0.);
                            }
                            else if diff.magnitude() < mobile.max_speed {
                                vel.vel = diff;
                                agent.orders.push_front(Order::MoveTo(tg_pos));
                            }
                            else {
                                vel.vel = diff.normalize_to(mobile.max_speed);
                                agent.orders.push_front(Order::MoveTo(tg_pos));
                            }
                        }
                    },
                }
            }
        }
    }
}
