use {
    cgmath::{
        prelude::*,
        Matrix4,
        Perspective,
        Vector3,
        Point3,
        Rad,
        Vector2,
    },
};

use crate::{
    config::{WINDOW_WIDTH, WINDOW_HEIGHT},
};

#[derive(Debug, Clone)]
pub struct Camera {
    pub screen_size: Vector2<f32>,
    pub eye: Point3<f32>,
    pub up: Vector3<f32>,
    pub dir: Vector3<f32>,
}

impl Default for Camera {
    fn default() -> Camera {
        Camera {
            screen_size: Vector2::new(WINDOW_WIDTH as f32, WINDOW_HEIGHT as f32),
            eye: Point3::new(0., 0., 0.),
            up: Vector3::new(0., -1., 0.),
            dir: Vector3::new(0., 0., -1.),
        }
    }
}

impl Camera {
    pub fn view_matrix(&self) -> Matrix4<f32> {
        Matrix4::look_at_dir(self.eye, self.dir, self.up)
    }

    pub fn perspective_matrix(&self) -> Matrix4<f32> {
        let near = 0.1;
        let far = 2500.;
        let aspect = self.screen_size.x / self.screen_size.y;
        let ymax = near * Rad::tan(Rad(0.5 / 2.));
        let xmax = ymax * aspect;
        Perspective {
            left: -xmax,
            right: xmax,
            bottom: -ymax,
            top: ymax,
            near,
            far,
        }.into()
    }

    pub fn inverse_perspective_matrix(&self) -> Matrix4<f32> {
        self.perspective_matrix().invert().unwrap()
    }

    pub fn inverse_view_matrix(&self) -> Matrix4<f32> {
        self.view_matrix().invert().unwrap()
    }
}
