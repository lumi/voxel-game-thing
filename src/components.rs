use {
    std::collections::VecDeque,
    specs::prelude::*,
    cgmath::{Point3, Vector3, Point2},
    luminance::tess::Tess,
};

use crate::{
    agent::Order,
    prim::Shape,
    thread_ref::ThreadRef,
};

#[derive(Clone, Debug)]
pub struct Pos {
    pub pos: Point3<f32>,
}

impl Component for Pos {
    type Storage = VecStorage<Self>;
}

#[derive(Clone, Debug)]
pub struct Vel {
    pub vel: Vector3<f32>,
}

impl Component for Vel {
    type Storage = VecStorage<Self>;
}

pub struct HasMesh {
    pub tess: Option<ThreadRef<Tess>>,
    pub color: [u8; 3],
}

impl Component for HasMesh {
    type Storage = VecStorage<Self>;
}

#[derive(Clone, Debug)]
pub struct Agent {
    pub orders: VecDeque<Order>,
}

impl Component for Agent {
    type Storage = VecStorage<Self>;
}

#[derive(Clone, Debug)]
pub struct Mobile {
    pub max_speed: f32,
}

impl Component for Mobile {
    type Storage = VecStorage<Self>;
}

#[derive(Clone, Debug)]
pub struct Solid {
    pub shape: Shape,
}

impl Component for Solid {
    type Storage = VecStorage<Self>;
}

#[derive(Clone, Debug)]
pub struct IsChunk {
    pub pos: Point2<i32>,
}

impl Component for IsChunk {
    type Storage = VecStorage<Self>;
}
