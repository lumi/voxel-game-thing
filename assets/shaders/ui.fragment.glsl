uniform sampler2D tex;

in vec2 v_uv;
in vec4 v_color;
flat in uint v_mode;

out vec4 frag;

void main() {
  if (v_mode == uint(0)) { // Texture
    vec4 s = texture(tex, v_uv);
    frag = vec4(s.rgb, min(s.a, v_color.a));
  }
  else if (v_mode == uint(1)) { // Solid color
    frag = v_color;
  }
  else { // Error
    frag = vec4(1., 0., 0., 1.);
  }
}
