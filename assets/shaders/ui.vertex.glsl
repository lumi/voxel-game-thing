uniform mat3 transform;
uniform vec2 window_size;

in vec2 co;
in vec2 uv;
in vec4 color;
in uint mode;

out vec2 v_uv;
out vec4 v_color;
flat out uint v_mode;

void main() {
  vec3 transformed = transform * vec3(co, 1.);
  vec2 scaled = transformed.xy / window_size * 2. - 1.;
  vec2 flipped = vec2(scaled.x, -scaled.y);
  gl_Position = vec4(flipped, 0., 1.);
  v_uv = uv;
  v_color = color;
  v_mode = mode;
}
