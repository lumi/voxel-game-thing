uniform vec3 light;

in vec2 v_uv;
in vec3 v_color;
in vec3 v_normal;

out vec4 frag;

void main() {
  vec3 color;
  if(v_uv.x < 0.05 || v_uv.y < 0.05 || v_uv.x > 0.95 || v_uv.y > 0.95) {
    color = vec3(0.1, 0.1, 0.1);
  }
  else {
    color = v_color;
  }
  float factor = dot(light, v_normal);
  frag = vec4(factor * .3 * color + .7 * color, 1.);
}
