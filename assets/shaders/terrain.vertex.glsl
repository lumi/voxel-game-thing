uniform mat4 view;
uniform mat4 perspective;
uniform mat4 model;

in vec3 co;
in vec3 normal;
in vec2 uv;
in vec3 color;

out vec2 v_uv;
out vec3 v_color;
out vec3 v_normal;

void main() {
  gl_Position = perspective * view * model * vec4(co, 1.);
  v_uv = uv;
  v_color = color;
  v_normal = normal;
}
