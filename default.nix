{ pkgs ? import <nixpkgs> {}
, stdenv ? pkgs.stdenv
}:

stdenv.mkDerivation rec {
  pname = "voxel-game-thing";
  version = "0.0.1";
  nativeBuildInputs = [ pkgs.gcc ];
  buildInputs = [ pkgs.xorg.libX11 pkgs.xorg.libXcursor pkgs.xorg.libXrandr pkgs.xorg.libXi pkgs.libGL ];
  shellHook = ''
    export LD_LIBRARY_PATH="${stdenv.lib.makeLibraryPath buildInputs}:$LD_LIBRARY_PATH"
  '';
}
